from .models import RealEstate, Property

from rest_framework import serializers


class RealEstateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RealEstate
        fields = [
            'id',
            'name',
            'address'
        ]


class PropertySerializer(serializers.ModelSerializer):
    real_estate = RealEstateSerializer(read_only=True)

    class Meta:
        model = Property
        fields = [
            'id',
            'name',
            'address',
            'description',
            'status',
            'characteristics',
            'type',
            'goal',
            'real_estate'
        ]


class PropertyCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        fields = [
            'id',
            'name',
            'address',
            'description',
            'status',
            'characteristics',
            'type',
            'goal',
            'real_estate'
        ]
