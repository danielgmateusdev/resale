from .models import RealEstate, Property
from .serializers import RealEstateSerializer, PropertySerializer, PropertyCreateUpdateSerializer

from rest_framework.viewsets import ModelViewSet


class RealEstate(ModelViewSet):
    queryset = RealEstate.objects.all()
    http_method_names = ['get', 'post', 'put', 'delete']
    serializer_class = RealEstateSerializer


class Property(ModelViewSet):
    queryset = Property.objects.all()
    http_method_names = ['get', 'post', 'put', 'delete']
    default_serializer_class = PropertySerializer
    serializer_classes = {
        'create': PropertyCreateUpdateSerializer,
        'update': PropertyCreateUpdateSerializer
    }

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)
