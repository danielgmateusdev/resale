from django.db import models


class RealEstate(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Name:', max_length=35)
    address = models.CharField('Address:', max_length=150, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Real Estate'
        verbose_name_plural = 'Real Estate'
        ordering = ['-id']

    def __str__(self):
        return self.name  

    
class Property(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Name:', max_length=35)
    address = models.CharField('Address:', max_length=150)
    description = models.CharField('Description:', max_length=150)
    status = models.CharField('Status:', max_length=7, choices=(('Ativo', 'Ativo'), ('Inativo', 'Inativo')))
    characteristics = models.TextField('Characteristics', blank=True, null=True)
    type = models.CharField('Type:', max_length=11, choices=(('Casa', 'Casa'), ('Apartamento', 'Apartamento')))
    goal = models.CharField('Goal:', max_length=11, choices=(('Residencial', 'Residencial'),
                                                             ('Escritório', 'Escritório')))
    real_estate = models.ForeignKey(RealEstate, related_name='properties', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Property'
        verbose_name_plural = 'Property'
        ordering = ['-id']

    def __str__(self):
        return self.name 
