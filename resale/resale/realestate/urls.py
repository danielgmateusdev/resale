from .views import RealEstate, Property

from rest_framework.routers import SimpleRouter

app_name = 'realestate'

router = SimpleRouter()
router.register('realestate', RealEstate)
router.register('property', Property)
