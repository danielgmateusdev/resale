"""resale URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.conf.urls.static import static

from . import settings

from rest_framework.routers import DefaultRouter
from rest_framework import permissions

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from resale.realestate.urls import router as realestate_router

schema_view = get_schema_view(
    openapi.Info(
        title="Resale API",
        default_version='v1',
        description="Resale API v1",
        contact=openapi.Contact(email="danielgmateus.dev@gmail.com")
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.registry.extend(realestate_router.registry)

urlpatterns = [
    path('v1/', include(router.urls)),
    path('v1/docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
