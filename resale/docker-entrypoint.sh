#!/bin/bash -x

mkdir ./static

python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput

gunicorn resale.wsgi:application --bind 0.0.0.0:8082

exec "$@"