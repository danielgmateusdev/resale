# api-resale

API para o cadastro de imóveis. Foi desenvolvida utilizando Django Rest e MySQL. 

Para utilizar este projeto é necessário ter o Docker instalado em sua máquina.

1) Para iniciar o projeto, utilize o comando abaixo:

```
docker-compose up -d --build
```

2) Para visualizar a documentação da API, acesse o endereço abaixo:

```
http://localhost:8082/v1/docs
```

Caso não deseje utilizar em sua máquina é possível acessar através do endpoint abaixo.

```
https://api.danielgmateus.com.br/v1/
```

Para acessar a documentação, utilize o endereço abaixo.
```
https://api.danielgmateus.com.br/v1/docs/
```

**Obs:** Na pasta utils, você encontrará um arquivo com o nome API.json que possui os endpoints e exemplos para ser importado no Insomnia.